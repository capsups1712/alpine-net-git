FROM mcr.microsoft.com/dotnet/core/sdk:3.1-alpine

RUN apk --update add git less openssh curl \
    && curl -sLO https://github.com/git-lfs/git-lfs/releases/download/v2.13.3/git-lfs-linux-amd64-v2.13.3.tar.gz \
    && mkdir -p unpacked \
    && tar zxvf git-lfs-linux-amd64-v2.13.3.tar.gz -C unpacked \
    && mv unpacked/git-lfs /usr/bin/ \
    && rm -rf unpacked \
    && rm -rf git-lfs-linux-amd64-v2.13.3.tar.gz \
    && rm -rf /var/lib/apt/lists/* \
    && rm /var/cache/apk/*